#pragma once

#include <string>

// entry point - calls on the back end as well
void front_end(std::string_view file_name);